'''
@file       Lab01.py

@brief      This file is a finite state machine for the Vendotron ˆTMˆ.

@details    Lab01 is a finite state machine (FSM) consisting of 3 states and an initialization state.
            These states are:

            State0: Initialization
            State1: Standby
            State2: Selection Handler
            State3: Eject

            Using these states the vending machine is able to display a welcome message, receive
            coins from the user, receive a selection, and either dispense the selected drink
            and return leftover change, or if funds are insufficient, it will give an error and 
            display the price of the selection. 

@image      

'''

import keyboard
#import os

# this is the state variable, it initializes to the first state 
state = 0


def getKey(key):
    """
    @brief getKey is a callback for storing keys pressed as a variable
    
    @details All keyboard interrupts are set up to call getKey. The last key pressed is passed 
    to getKey, the name of which is then stored in a variable to be used by the rest of the program
    
    @param key The keyboard event that triggered the callback
    
    @return The name of the keyboard event which triggered the callback is stored in pushed_key as a string

    """
    global pushed_key
    
    # Take the name of the function that 
    pushed_key = key.name
   
def getBalance(balance):
    '''
    @brief      Takes list of coins returns total value

    @details    Multiplies each denomination by it's value in cents and adds each denomination to get a total value

    @param      balance Either a list or a tuple containing an interger number of each of the following denominations
                    0. pennies
                    1. nickels
                    2. dimes
                    3. quarters
                    4. ones
                    5. fives
                    6. tens
                    7. twenties

    @return     int_balance An interger value representing the toal value of the balance in cents
    '''

    # convert coins into their total value in cents
    int_balance = balance[0] + balance[1]*5 + balance[2]*10 + balance[3]*25 + balance[4]*100 + balance[5]*500 + balance[6]*1000 + balance[7]*2000

    return(int_balance)

def getChange(price, payment):
    '''
    @brief          Computes the least number of denominations to make change
    
    @details        Given an interger as a price and a set of coins and bills as a 
                    tuple, compute the change in least number of coins/bills.
    
    @param price    The price of the item as an interger number of cents
    
    @param payment  A tuple describing the quantity of each denomination in 
                    ascending order
    
    @return         If funds are sufficient, returns a tuple containing quantities of 
                    each denomination required to make change

    '''
    
    # compute the interger value of change
    int_change = getBalance(payment) - price

    global change

    # if the price is greater than the payment, return -1
    if int_change < 0:
        change = -1
        return
    
    # if the price and payment are equal return none
    if int_change == 0:
        change = None
        return

    change = [0,0,0,0,0,0,0,0]

    # systematically subtract the largest denomination possible and store each subtraction in the change list 
    while int_change > 0:
        # if greater than 20, subtract 20
        if int_change >= 2000:
            change[7] +=1
            int_change -= 2000

        # if greater than 10, subtract 10
        elif int_change >= 1000:    
            change[6] += 1
            int_change -= 1000
        
        # if greater than 5, subtract 5
        elif int_change >= 500:    
            change[5] += 1
            int_change -= 500
        
        # if greater than 1, subtract 1
        elif int_change >= 100:    
            change[4] += 1
            int_change -= 100
        
        # if greater than 0.25, subtract 0.25
        elif int_change >= 25:    
            change[3] += 1
            int_change -= 25
            
        # if greater than 0.10, subtract 0.10
        elif int_change >= 10:    
            change[2] += 1
            int_change -= 10
        
        # if greater than 0.05, subtract 0.05
        elif int_change >= 5:    
            change[1] += 1
            int_change -= 5
            
        # if greater than 0.01, subtract 0.01
        elif int_change >= 1:    
            change[0] += 1
            int_change -= 1
    
    change = tuple(change)

if __name__ == "__main__":
    # set up keyboard callback    
    keyboard.on_release(getKey)
   
    # initialize variables
    pushed_key = None
    change = None
    selection = None
    change = None
    price = 0
    int_balance = 0
    balance = [0, 0, 0, 0, 0, 0, 0, 0]

    while True:
        try:
            """Implement FSM using a while loop and an if statement
            will run eternally until user presses CTRL-C
            """
        
            if state == 0:
                """
                Prints a VendotronˆTMˆ welcome message with beverage prices
                """
                #os.system('cls')
                print("Hello! Might I tempt you with a can of sugar? I've got Cuke for $2.99, Popsi for $2.98, Spryte for $0.01, and Dr. Pupper for $143.43" )
                state = 1 # on the next iteration, the FSM will run state 1
        
            elif state == 1:
                """
                Perform standby operations
                """
                if pushed_key:

                    #Handle coins added
                    if pushed_key in ('0','1','2','3','4','5','6','7'):
                        
                        # pennies
                        if pushed_key == '0':
                            balance[0] += 1
                        # nickels
                        elif pushed_key == '1':
                            balance[1] += 1
                        # dimes
                        elif pushed_key == '2':
                            balance[2] += 1
                        # quarters
                        elif pushed_key == '3':
                            balance[3] += 1
                        # ones
                        elif pushed_key == '4':
                            balance[4] += 1
                        # fives
                        elif pushed_key == '5':
                            balance[5] += 1
                        # tens
                        elif pushed_key == '6':
                            balance[6] += 1
                        # twenties
                        elif pushed_key == '7':
                            balance[7] += 1
                        
                        #os.system('cls')
                        print("Balance: ${}".format(str(getBalance(balance)/100)))

                    #Handle Selections
                    elif pushed_key == 'c':
                        selection = "Cuke"
                        state = 2

                    elif pushed_key == 'p':
                        selection = "Popsi"
                        state = 2

                    elif pushed_key == 's':
                        selection = "Spryte"
                        state = 2

                    elif pushed_key == 'd':
                        selection = "Dr. Pupper"
                        state = 2
                    
                    #Handle Ejection
                    elif pushed_key == 'e':
                        state = 3

                    else:
                        print("Invalid Input")    
                    pushed_key = None

            elif state == 2:
                """
                Handle selections
                """
                # get the appropriate price for the selection
                if selection == "Cuke":
                    price = 299
                elif selection == "Popsi":
                    price = 298
                elif selection == "Spryte":
                    price = 1
                elif selection == "Dr. Pupper":
                    price = 14343
                else:
                    break

                payment = tuple(balance)

                getChange(price, payment)
                
                # if balance is insufficient, return an error
                if change == -1:
                    #os.system('cls')
                    print("Balance Insufficient, the price of", selection, "is ${}".format(price/100))
                    print("Your current balance is ${}".format(str(getBalance(balance)/100)))
                    state = 1
                
                # if payment is sufficient do the following
                else:
                    #os.system('cls')

                    print("Enjoy your",selection + "!")

                    # if there is leftover change, ask the user if they want another drink
                    if change:
                        balance = [x for x in change]
                        print("You have a remaining balance of ${}, please make another selection or press E for change.".format(str(getBalance(balance)/100)))
                        state = 1

                    # if there is no leftover change, clear the balance and restart Vendotron
                    else:
                        balance = [0 for index in balance]
                        state = 0
            
            if state == 3:
                '''
                Eject state
                '''
                # compute change by using getChange with a price of 0
                price = 0
                payment = (balance[0], balance[1], balance[2], balance[3], balance[4], balance[5], balance[6], balance[7])
                getChange(price, payment)

                # zero out balance
                balance = [0, 0, 0, 0, 0, 0, 0, 0]

                # print change in fewest possible denominations
                print("Dont forget your change:", change)
                state = 0

        except KeyboardInterrupt:
            break
    # signify that we have exited safely
    print("Bye")

    # unhook the keyboard
    keyboard.unhook_all()
