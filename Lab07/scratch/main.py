'''
To connect a pin to V_DD, set the pin up as a push/pull mode
(open drain -- Can pull to ground, or not do that)
(high Z <- input impedence -- High 'resistance'. sensor wont know its being read )

Create an ADC object to create a highZ 

Need new pyb.ADC every time you need to change a pin, which 
must happen hundreds of times

import gc
gc.collect() (garbage collect)

Dumb delay loops are dumb because they block,
but they are OK for microsecond delays usually.
We only switch between states on milliseconds
(use tick difs)

'''
import pyb
from scratchDriver2 import touchPad

tch = touchPad(1,1,1,"PA7","PA1","PA6","PA0")

while True:    
    print(tch.getY())

