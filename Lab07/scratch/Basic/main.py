'''
To connect a pin to V_DD, set the pin up as a push/pull mode
(open drain -- Can pull to ground, or not do that)
(high Z <- input impedence -- High 'resistance'. sensor wont know its being read )

Create an ADC object to create a highZ 

Need new pyb.ADC every time you need to change a pin, which 
must happen hundreds of times

import gc
gc.collect() (garbage collect)

Dumb delay loops are dumb because they block,
but they are OK for microsecond delays usually.
We only switch between states on milliseconds
(use tick difs)

'''
import pyb
import utime

ym = pyb.Pin(pyb.Pin.board.PA0)
xm = pyb.Pin(pyb.Pin.board.PA1)
yp = pyb.Pin(pyb.Pin.board.PA6)
xp = pyb.Pin(pyb.Pin.board.PA7)

xp.high()
xm.low()
yp.high()
ym.low()

while True:
    #initialize pins for x scan
    xp.high()
    xm.init(xm.OUT_PP)
    xp.init(xp.OUT_PP) 
    ym.init(ym.IN, ym.PULL_DOWN)
    yp.init(yp.IN, yp.PULL_DOWN)

    adc = pyb.ADC(ym)
    
    #sleep to allow signal to settle
    #utime.sleep_ms(1)

    #scan x
    x = adc.read()

    #reset pin values
    xp.low()

    #initialize pins for y scan
    yp.high()
    ym.init(ym.OUT_PP)
    yp.init(yp.OUT_PP)
    xm.init(xm.IN, xm.PULL_DOWN)
    xp.init(xp.IN, xp.PULL_DOWN)

    adc = pyb.ADC(xm)
    
    #sleep to allow signal to settle
    #utime.sleep_ms(1)

    #scan y
    y = adc.read()
    
    #reset pin values
    yp.low()
    

    yp.high()
    yp.init(yp.OUT_PP)
    xm.init(xm.OUT_PP)
    ym.init(ym.IN, ym.PULL_DOWN)
    xp.init(xp.IN, xp.PULL_DOWN)
    
    adc = pyb.ADC(ym)
    
    #sleep to allow signal to settle
    #utime.sleep_ms(1) 
 
    #scan z
    z = adc.read()

    #reset pin values
    yp.low()


    #print result
    print(x,y,z)

'''while True:
    vals[ind] = adc.read()
    ind +=1
    if ind >= length:
        ind = 0
        
        print(round(sum(vals)/(len(vals)*100)))
    '''