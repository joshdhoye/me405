'''
@file       Temp_Driver.py

@brief      

@details    
'''

import pyb
import utime

class touchPad:
    '''
    @brief      

    @details    
    '''

    def __init__(self, xLen, yLen, origin, xpPin, xmPin, ypPin, ymPin):
        '''
        @brief initializes I2C comminication and inital variables
        
        @param address input address of mcp9808 module. This value is 24 in decimal by default but can be altered using A0, A1, and A2 pins
        '''

        self.xm = pyb.Pin(xmPin)
        self.xp = pyb.Pin(xpPin)
        self.yp = pyb.Pin(ypPin)
        self.ym = pyb.Pin(ymPin)

        self.xp.high()
        self.xm.low()
        self.yp.high()
        self.ym.low()

        ## origin is a tuple containing the x and y coordinates of the center of the plate
        self.origin = origin

        ##
        self.xLen = xLen

        ##
        self.yLen = yLen

        # variables for rolling averages
        histLen = 100
        self.hist = [[0 for index in range(histLen)],[0 for index in range(histLen)],[0 for index in range(histLen)]]
        self.index = 0

        # set up the timer channel
        PER = 0x7FFFFFFF
        self.tim2 = pyb.Timer(2, period = PER, prescaler = 79)

    def getZ(self):
        '''
        @brief
        
        '''

        self.yp.init(self.yp.OUT_PP)
        self.xm.init(self.xm.OUT_PP)
        self.xp.init(self.xp.IN, self.xp.PULL_DOWN)
        adc = pyb.ADC(self.ym)

        #utime.sleep_us(4)  
        z = adc.read()

        #self.yp.low()
        
        return z
    
    
    def getX(self):
        '''
        @brief
        
        '''

        self.xp.init(self.xp.OUT_PP)
        self.xm.init(self.xm.OUT_PP)
        self.yp.init(self.yp.IN, self.yp.PULL_DOWN)
        adc = pyb.ADC(self.ym)
        
        #utime.sleep_us(4)  
        x = adc.read() 
        
        return x

        #return sum(self.hist[0])/len(self.hist[0])    
        

    def getY(self):
        '''
        @brief
        
        '''       
        self.yp.init(self.yp.OUT_PP)
        self.ym.init(self.ym.OUT_PP)
        self.xp.init(self.xp.IN, self.xp.PULL_DOWN)
        adc = pyb.ADC(self.xm)

        #utime.sleep_us(4)  
        y = adc.read()

        return y

    def getXYZ(self):
        '''
        @brief
        
        ''' 
        z = self.getZ()
        if z < 4060:    
            y = self.getY()  
            x = self.getX()
        else:
            y = 0
            x = 0
        return (x,y,z)

if __name__ == "__main__":
    tch = touchPad(1,1,1,"PA7","PA1","PA6","PA0")
    '''ind = 0
    times = [0]
    while ind <= 100:
        t1 = utime.ticks_us()
        tch.getXYZ()
        t2 = utime.ticks_us()
        total_time = utime.ticks_diff(t2, t1)
        times.append(int(float(total_time)))
        ind+=1
    print(tch.getXYZ())
    print(times)
    '''
    while True:
        t1 = utime.ticks_us()
        XYZ = (tch.getXYZ())
        t2 = utime.ticks_us()
        total_time = utime.ticks_diff(t2, t1)
        print(XYZ,int(float(total_time)))