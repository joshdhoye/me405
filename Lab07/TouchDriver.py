'''
@file       TouchDriver.py

@brief      This file defines a class for obtaining positional data from the touchpad.

@details    This file can be imported as a module to use the touchpad class or run on its own as it
            contains test code in the 'if __name__ == "__main__"' block located at the bottom of the code.
'''

import pyb
import utime

class touchpad:
    '''
    @brief      This class contains functions for obtaining positional data from the touchpad.

    @details    The class, touchpad, initializes with paramaters with the length and width dimensions of the
                touchpad in millimeters, followed by the x and y distance from the edge to the center of the pannel,
                and the four pins to be used to probe the touchpad. This is done by energizing one axis of 
                the touchpad, which operates as a matrix of voltage dividers, and reading from the opposide axis.
                The class contains functions for reading each axis, along with a "Z" which denotes whether or not there
                is contact at all by measuring the voltage across the two axis.
    '''

    def __init__(self, xLen, yLen, centerX, centerY, xpPin, xmPin, ypPin, ymPin):
        '''
        @brief This function initializes I2C comminication and inital variables
        
        @param xLen This parameter is used to call out the length of the touchpad. For our usecase this is millimeters,
        but will work for any unit as long as the parameters are consistant

        @param yLen This parameter is used to call out the width of the touchpad.

        @param centerX This parameter is used to call out the desired x-axis origin of the touchpad as measured from the edge.

        @param centerY This parameter is used to call out the desired y-axis origin of the touchpad as measured from the edge.
        
        @param xpPin This variable defines the pin used to energize the x axis, corresponding to the X+ pin on the module.
        
        @param xmPin This variable defines the pin used to ground the x axis and to read the y axis. Corresponds to the X- pin on the module.

        @param ypPin This variable defines the pin used to energize the y axis, corresponding to the X+ pin on the module.

        @param ymPin This variable defines the pin used to ground the y axis and to read the x axis. Corresponds to the Y- pin on the module.
        '''

        # degine each of the 
        self.xm = pyb.Pin(xmPin)
        self.xp = pyb.Pin(xpPin)
        self.yp = pyb.Pin(ypPin)
        self.ym = pyb.Pin(ymPin)

        self.xp.high()
        self.xm.low()
        self.yp.high()
        self.ym.low()

        ## centerX is used to call out the desired x-axis origin of the touchpad as measured from the edge
        self.centerX = centerX

        ## centerY is used to call out the desired y-axis origin of the touchpad as measured from the edge
        self.centerY = centerY

        ## xLen is used to call out the length of the touchpad
        self.xLen = xLen

        ## yLen is used to call out the width of the touchpad
        self.yLen = yLen

        # variables for rolling averages
        self.histLen = 10
        self.hist = [[0 for index in range(self.histLen)],[0 for index in range(self.histLen)],[0 for index in range(self.histLen)]]
        self.index = 0

        # experimentally determined minimum and maximum ADC outputs for x and y coordinates
        self.x_max = 3700
        self.x_min = 200

        self.y_max = 3600
        self.y_min = 460

    def getZ(self):
        '''
        @brief  This function is used to check for contact on the z axis of the touchpad.
        
        '''

        # set yp and xm to outputs so we can measure the voltage across them
        self.yp.init(self.yp.OUT_PP)
        self.xm.init(self.xm.OUT_PP)
        # set xp to high Z mode so as to not affect the ADC reading
        self.xp.init(self.xp.IN, self.xp.PULL_DOWN)
        # make ym an ADC pin so we can measure the voltage across the axes
        adc = pyb.ADC(self.ym)

        # allow the voltages to stabalize before reading them
        utime.sleep_us(4)  

        # check if there is a voltage drop between X and Y and return true if so, signifying that contact is made on the touchpad
        if adc.read() < 4060:
            z = 1
        else:
            z = 0
        
        # return the result
        return z
    
    def getX(self):
        '''
        @brief  This function is used to scan the X axis of the touchpad.
        
        '''
        # set xp and xm to outputs in order to energize the x axis
        self.xp.init(self.xp.OUT_PP)
        self.xm.init(self.xm.OUT_PP)
        # set yp to high z and ym to ADC to enable reading of the x component
        self.yp.init(self.yp.IN, self.yp.PULL_DOWN)
        adc = pyb.ADC(self.ym)
        
        # allow the voltages to stabalize before reading them
        utime.sleep_us(4)  

        # store current ADC value in a rolling list
        self.hist[0][self.index] = adc.read() 
        
        # compute the average of the last few x values to filter outliers
        filtered_x = sum(self.hist[0])/self.histLen

        # transform the filtered x component into appropriate length units
        transformed_x = (filtered_x-self.x_min)* ((self.xLen)/(self.x_max-self.x_min)) - self.centerX

        # return the result
        return(transformed_x)
        

    def getY(self):
        '''
        @brief  This function is used to scan the Y axis of the touchpad.
        
        '''       
        # set yp and ym to outputs in order to energize the y axis
        self.yp.init(self.yp.OUT_PP)
        self.ym.init(self.ym.OUT_PP)
        # set xp to high z and xm to ADC to enable reading of the y component
        self.xp.init(self.xp.IN, self.xp.PULL_DOWN)
        adc = pyb.ADC(self.xm)

        # allow the voltages to stabalize before reading them
        utime.sleep_us(4)  

        # store current ADC value in a rolling list
        self.hist[1][self.index] = adc.read() 

        # compute the average of the last few y values to filter outliers
        filtered_y = sum(self.hist[1])/self.histLen

        # transform the filtered y component into appropriate length units
        transformed_y = (filtered_y-self.y_min)* ((self.yLen)/(self.y_max-self.y_min)) - self.centerY

        # return the result
        return(transformed_y)

    def getXYZ(self):
        '''
        @brief  This function is used to read the Z axis, and the X and Y axes as necessary.
        
        ''' 
        # read the z axis
        z = self.getZ()

        # if there is contact on the touchpad,
        if z == 1:    
            # increment the rolling average index
            self.index += 1
            if self.index >= 10:
                self.index = 0
            
            # read the x and y axes
            y = self.getY()  
            x = self.getX()

        # if there is no contact, simply set x and y to 0
        else:
            y = 0
            x = 0

        # return all three coordinates as a tuple
        return (x,y,z)

# test code
if __name__ == "__main__":
    tch = touchpad(180,100,90,50,"PA7","PA1","PA6","PA0")
    
    while True:
        t1 = utime.ticks_us()
        XYZ = (tch.getXYZ())
        t2 = utime.ticks_us()
        total_time = utime.ticks_diff(t2, t1)
        print(XYZ,int(float(total_time)))