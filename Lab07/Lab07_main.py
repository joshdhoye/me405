'''
To connect a pin to V_DD, set the pin up as a push/pull mode
(open drain -- Can pull to ground, or not do that)
(high Z <- input impedence -- High 'resistance'. sensor wont know its being read )

Create an ADC object to create a highZ 

Need new pyb.ADC every time you need to change a pin, which 
must happen hundreds of times

import gc
gc.collect() (garbage collect)

Dumb delay loops are dumb because they block,
but they are OK for microsecond delays usually.
We only switch between states on milliseconds
(use tick difs)

'''
import pyb
import utime

ym = pyb.Pin(pyb.Pin.board.PA0)
xm = pyb.Pin(pyb.Pin.board.PA1)
yp = pyb.Pin(pyb.Pin.board.PA6)
xp = pyb.Pin(pyb.Pin.board.PA7)



xp.high()
xm.low()
adc = pyb.ADC(ym)


while True:
    '''
    ym.init(ym.IN)
    xm.init(xm.OUT_PP)
    yp.init(yp.IN)
    xp.init(xp.OUT_PP)
    '''
    xm.init(xm.OUT_PP)
    xp.init(xp.OUT_PP) 
    ym.init(ym.IN, ym.PULL_DOWN)
    yp.init(yp.IN, yp.PULL_DOWN)

    xp.high()
    xm.low()
    adc = pyb.ADC(ym)
    utime.sleep_ms(1)
    x = adc.read()

    #xp.low()

    ym.init(ym.OUT_PP)
    yp.init(yp.OUT_PP)
    xm.init(xm.IN, xm.PULL_DOWN)
    xp.init(xp.IN, xp.PULL_DOWN)
    
    yp.high()
    ym.low()
    adc = pyb.ADC(xm)

    utime.sleep_ms(1)
    y = adc.read()
        
    #yp.low()

    print(x,y)

'''while True:
    vals[ind] = adc.read()
    ind +=1
    if ind >= length:
        ind = 0
        
        print(round(sum(vals)/(len(vals)*100)))
    '''