'''
@file       Lab03_main.py

@author     Joshua Hoye

@brief      Keeps track of the ADC and generates an output when a good step response is recorded

@details    This file runs as the main file on the NUCLEO. It keeps a rolling list of ADC voltage outputs
            and corresponding timer counts from the button pin. When a valid step response is detected,
            the data is compiled and written to the UART.

@copyright

@date       January 26, 2021
'''


import array
import pyb
from pyb import UART

# initialize UART
myuart = UART(2)

# initialize ADC 
pinA0 = pyb.Pin(pyb.Pin.board.PA0)
adc = pyb.ADC(pinA0)

# this variable defines how many data points will be kept in the rolling data set
hlenth = 25

# these variables hold the rolling ADC and timer data
adcHist = array.array('H', (4095 for index in range(hlenth)))
timeHist = array.array('H', (0 for index in range(hlenth)))

# set up the timer 2 channel
PER = 0x7FFFFFFF
tim2 = pyb.Timer(2, period = PER, prescaler = 79)

# these varibale allows us to index through the rolling list
hindex = 0
hnext = 1

# this variable is put high by the user interface
gPressed = False

while True:
    
    # step once through the index
    hindex = hnext
    if hindex+1 >= hlenth:
        hnext = 0
    else:
        hnext = hindex+1

    # store new data in rolling history
    adcHist[hindex] =  adc.read() #
    timeHist[hindex] = tim2.counter()

    # if we have received something from the user interface, check if it is a "g"        
    if myuart.any() != 0:
        keyPressed = myuart.readline().decode('ascii')
        if keyPressed == 'g':
            gPressed = True

    # if the user wants a response, write one as soon as we have a good one
    if gPressed:  
        if adcHist[hindex] >= 4060 and adcHist[hnext] <= 10:
            hist = adcHist[hnext:] + adcHist[:hnext] + timeHist[hnext:] + timeHist[:hnext]
            myuart.write(str(hist))
            gPressed = False
