'''
@file       Lab03_UI_front.py

@author     Joshua Hoye

@brief      This file is the user interface for "Pushing the Right Buttons"

@details    Lab03_UI_front runs on the PC and acts as the user interface for "Pushing the Right Buttons".
            This file communicates with Lab03_main through a serial port. The main function of this
            file is to allow the user to request a time response from the main file, reeive that data
            and then plot for the user.

@copyright

@date       January 26, 2021
'''



import serial
import keyboard
import matplotlib.pyplot as plt

# initialize serial
ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)

def getKey(key):
    """
    @brief getKey is a callback for storing keys pressed as a variable
    
    @details All keyboard interrupts are set up to call getKey. The last key pressed is passed 
    to getKey, the name of which is then stored in a variable to be used by the rest of the program
    
    @param key The keyboard event that triggered the callback
    
    @return The name of the keyboard event which triggered the callback is stored in pushed_key as a string

    """
    global pushed_key
    pushed_key = key.name

# initialize variables
pushed_key = None

# set up keyboard interrupts
keyboard.on_release(getKey)

while True:
    try:
        # if a keys release is detected, check if it is a g 
        if pushed_key:
            if pushed_key == 'g':
                ser.write(pushed_key.encode('ascii'))
            else:
                pushed_key = None

        receive = ser.readline().decode('ascii')        # pull whatever may be on serial bus and store it in receive

        # if we got anything from serial, plot it
        if receive:
            
            # remove unwanted characers and parse into a list of strings
            result = receive.strip("array('H'), [").strip("]").split(', ')  
            
            # make the strings into intergers
            for index in range(len(result)):
                result[index] = int(result[index])

            plotADC = result[:(int(len(result)/2))]     # pull the ADC data from the list   
            plotADC = [x/1240 for x in plotADC]         # transform it into a voltage

            plotTime = result[(int(len(result)/2)):]    # pull the time data from the
            plotTime = [(x-plotTime[0])/1000 for x in plotTime] 

            # format plot
            plt.scatter(plotTime,plotADC)
            plt.xlabel("Time (ms)")
            plt.ylim([-.25,3.5])
            plt.ylabel("Voltage (V)")

            plt.show()

            with open('step_response.csv' , 'w') as file:
    
                # Adding column titles to file
                file.write('Time [ms],Voltage [V]\n')

                for index in range(int(len(result)/2)):
                    file.write(str(plotTime[index])+ ',' +str(plotADC[index])+ '\n')

    except KeyboardInterrupt:
        break


print('Bye')    # signify that we have exited cleanly
ser.close()     # close the serial port 