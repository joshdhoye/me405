'''
@file       Lab02.py

@author     Joshua Hoye

@brief      Lab02 tests user reaction time to a blinking light.

@details    This project tests the user's reaction time by meauring the time
            between the blinking of an LED and a user's button press. The LED
            blinks at random intervals between 2 and 3 seconds, and a button
            press triggers an interrupt which stores the reaction time. When 
            the program is ended via a keyboard interrupt, the average reaction
            time is calculated and presented to the user as a time in seconds.

@copyright

@date       January 26, 2021
'''

# Import needed libraries
import pyb
import utime
import random

def ButtonPress(IRQ_src):
    '''
    @author     Joshua Hoye

    @brief      When button is pressed, calculate reaction time

    @details    When the user button is pressed, the current timer count is taken
                and compared to the count when the LED blink occured. From this the 
                reaction time is calculated and stored in dt

    @param      IRQ_src is the interrupt event triggered by the button press

    @copyright

    @date       January 26, 2021
    '''

    global t1
    global t2
    global dt
    global newTime
    global pressMe

    # if we are expecting a button press at this time, do the following
    if pressMe:
        #store the current timer count and compute the difference
        t2 = tim2.counter()
        dt = t2-t1
        
        if dt <= 0: #Check for overflow
            dt += PER #if overflow occured, fix it
        
        pressMe = False
        newTime = True

# Initialize variables used in ButtonPress()
t1 = 0
t2 = 0
dt = 0

# set up the button bin and interrupt
pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=ButtonPress)

# set up the LED pin
pinA5 = pyb.Pin(pyb.Pin.board.PA5, mode = pyb.Pin.OUT_PP)

# set up the timer channel
PER = 0x7FFFFFFF
tim2 = pyb.Timer(2, period = PER, prescaler = 79)

randTime = random.randrange(2000000,3000000)
newTime = False
goodTimes = []
pressMe = 0 #ensures we dont get multiple attempts from one blink


while True:
    try:
        # set the program to sleep for a random time
        randTime = random.randrange(2000000,3000000)
        utime.sleep_us(randTime)
        
        # blink LED and record current count
        pinA5.high()
        t1 = tim2.counter()
        pressMe = 1 
        utime.sleep_us(1000)
        pinA5.low()
        
        # if we have a new reaction time, add it to the list
        if newTime == True:
            goodTimes.append(dt/1000000)
            newTime = False

    except KeyboardInterrupt:

        # if we have a new reaction time, add it to the list
        if newTime == True:
            goodTimes.append(dt/1000000)
            newTime = False
        
        break
try:

    # Compute and print response time appropriately
    if len(goodTimes) == 1:
        print("Your reaction time was {} seconds.".format(sum(goodTimes)/len(goodTimes)))
    else:
        print("Your average reaction time was {} seconds over {} attempts.".format(sum(goodTimes)/len(goodTimes),len(goodTimes)) )    
    
except ArithmeticError:
    # if we divide by 0, no valid attempts were recorded
    print("No valid attempts recorded.")
